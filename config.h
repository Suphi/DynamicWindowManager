/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int gappx     = 0;        /* gap pixel between windows */
static const unsigned int marginpx  = 0;        /* margin pixel around windows */
static const unsigned int snap      = 4;        /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray0[]       = "#111111";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#cccccc";
static const char col_gray4[]       = "#ffffff";
static const char col_blue[]        = "#215d9c";
static const char *colors[][3]      = {
	/*                fg         bg         border   */
	[SchemeNorm]  = { col_gray3, col_gray1, col_gray2 },
	[SchemeUnsel] = { col_gray3, col_gray0, col_gray2 },
	[SchemeSel]   = { col_gray4, col_blue,  col_blue  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/*class     instance    title                 tags mask     isfloating   monitor */
	{ NULL,     NULL,       "GIMP Startup",       0,            1,           -1 },
	{ NULL,     NULL,       "gcolor2",            0,            1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.60; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

/* first entry is default */
/* NULL layout function means floating behavior */
static const Layout layouts[] = { { tileleft }, { tileright }, { tileup }, { tiledown }, { monocle }, { NULL } };

/* key definitions */
#define MODKEY Mod4Mask
#define DOWNKEY XK_Down
#define LEFTKEY XK_Left
#define RIGHTKEY XK_Right
#define UPKEY XK_Up
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "/home/suphi/Documents/Scripts/dmenu-custom.sh", "-b", "-i", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_blue, "-sf", col_gray4, NULL };
static const char *dmenuallcmd[] = { "dmenu_run", "-b", "-i", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_blue, "-sf", col_gray4, NULL };
static const char *dmenuwirelesscmd[] = { "/home/suphi/Documents/Scripts/dmenu-wireless.sh", "-b", "-i", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_blue, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *browsercmd[] = { "chromium", NULL };
static const char *filecmd[] = { "thunar", NULL };
static const char *editorcmd[] = { "mousepad", NULL };
static const char *colorcmd[] = { "gcolor2", NULL };
static const char *playcmd[] = { "pragha", "-t", NULL };
static const char *stopcmd[] = { "pragha", "-s", NULL };
static const char *prevcmd[] = { "pragha", "-r", NULL };
static const char *nextcmd[] = { "pragha", "-n", NULL };
static const char *calculatorcmd[] = { "galculator", NULL };
static const char *printscreencmd[]  = { "xfce4-screenshooter", "-r", "-s", "/home/suphi/Pictures", NULL };
static const char *backlightdowncmd[] = { "/home/suphi/Documents/Scripts/backlight.sh", "-5", NULL };
static const char *backlightupcmd[] = { "/home/suphi/Documents/Scripts/backlight.sh", "+5", NULL };
static const char *touchpadcmd[] = { "/home/suphi/Documents/Scripts/touchpad.sh", NULL };
static const char *volumemutecmd[] = { "/home/suphi/Documents/Scripts/mute.sh", "toggle", NULL };
static const char *volumedowncmd[] = { "/home/suphi/Documents/Scripts/volume.sh", "-5%", NULL };
static const char *volumeupcmd[] = { "/home/suphi/Documents/Scripts/volume.sh", "+5%", NULL };
static const char *simplestatuscmd[] = { "dstatus", "s", "60", "/home/suphi/Documents/Scripts/simple.sh", NULL };
static const char *advancedstatuscmd[] = { "dstatus", "s", "1", "/home/suphi//Documents/Scripts/advanced.sh", NULL };

static Key keys[] = {
	/* modifier                        key                          function           argument */
	{ 0,                               XF86XK_MonBrightnessDown,    spawn,             {.v = backlightdowncmd } },
	{ 0,                               XF86XK_MonBrightnessUp,      spawn,             {.v = backlightupcmd } },
	{ 0,                               XF86XK_TouchpadToggle,       spawn,             {.v = touchpadcmd } },
	{ 0,                               XF86XK_AudioMute,            spawn,             {.v = volumemutecmd } },
	{ 0,                               XF86XK_AudioLowerVolume,     spawn,             {.v = volumedowncmd } },
	{ 0,                               XF86XK_AudioRaiseVolume,     spawn,             {.v = volumeupcmd } },
	{ 0,                               XF86XK_AudioPlay,            spawn,             {.v = playcmd } },
	{ 0,                               XF86XK_AudioStop,            spawn,             {.v = stopcmd } },
	{ 0,                               XF86XK_AudioPrev,            spawn,             {.v = prevcmd } },
	{ 0,                               XF86XK_AudioNext,            spawn,             {.v = nextcmd } },
	{ 0,                               XF86XK_WWW,                  spawn,             {.v = browsercmd } },
	{ 0,                               XF86XK_Launch1,              spawn,             {.v = dmenuwirelesscmd } },
	{ 0,                               XF86XK_WebCam,               spawn,             {.v = dmenuwirelesscmd } },
	{ 0,                               XK_Menu,                     spawn,             {.v = dmenucmd } },
	{ MODKEY,                          XK_Menu,                     spawn,             {.v = dmenuallcmd } },
	{ 0,                               XK_Print,                    spawn,             {.v = printscreencmd } },
	{ MODKEY,                          XK_t,                        spawn,             {.v = termcmd } },
	{ MODKEY,                          XK_b,                        spawn,             {.v = browsercmd } },
	{ MODKEY,                          XK_f,                        spawn,             {.v = filecmd } },
	{ MODKEY,                          XK_e,                        spawn,             {.v = editorcmd } },
	{ MODKEY,                          XK_c,                        spawn,             {.v = calculatorcmd } },
	{ MODKEY,                          XK_p,                        spawn,             {.v = colorcmd } },
	{ MODKEY|ShiftMask,                XK_s,                        spawn,             {.v = simplestatuscmd } },
	{ MODKEY|ShiftMask,                XK_a,                        spawn,             {.v = advancedstatuscmd } },
	/* modifier                        key                          function           argument */
	{ MODKEY,                          DOWNKEY,                     actiondown,        {.i = 0} },
	{ MODKEY|ShiftMask,                DOWNKEY,                     actiondown,        {.i = 1} },
	{ MODKEY|ControlMask,              DOWNKEY,                     actiondown,        {.i = 2} },
	{ MODKEY|ControlMask|ShiftMask,    DOWNKEY,                     setlayout,         {.v = &layouts[3]} },
	{ MODKEY,                          LEFTKEY,                     actionleft,        {.i = 0} },
	{ MODKEY|ShiftMask,                LEFTKEY,                     actionleft,        {.i = 1} },
	{ MODKEY|ControlMask,              LEFTKEY,                     actionleft,        {.i = 2} },
	{ MODKEY|ControlMask|ShiftMask,    LEFTKEY,                     setlayout,         {.v = &layouts[0]} },
	{ MODKEY,                          RIGHTKEY,                    actionright,       {.i = 0} },
	{ MODKEY|ShiftMask,                RIGHTKEY,                    actionright,       {.i = 1} },
	{ MODKEY|ControlMask,              RIGHTKEY,                    actionright,       {.i = 2} },
	{ MODKEY|ControlMask|ShiftMask,    RIGHTKEY,                    setlayout,         {.v = &layouts[1]} },
	{ MODKEY,                          UPKEY,                       actionup,          {.i = 0} },
	{ MODKEY|ShiftMask,                UPKEY,                       actionup,          {.i = 1} },
	{ MODKEY|ControlMask,              UPKEY,                       actionup,          {.i = 2} },
	{ MODKEY|ControlMask|ShiftMask,    UPKEY,                       setlayout,         {.v = &layouts[2]} },
	{ MODKEY,                          XK_Shift_R,                  togglebar,         {0} },
	{ MODKEY,                          XK_Return,                   togglemonocle,     {0} },
	{ MODKEY,                          XK_space,                    togglefloating,    {.i = 32} },
	{ MODKEY,                          XK_Page_Down,                focusmon,          {.i = -1 } },
	{ MODKEY|ShiftMask,                XK_Page_Down,                tagmon,            {.i = -1 } },
	{ MODKEY,                          XK_Page_Up,                  focusmon,          {.i = +1 } },
	{ MODKEY|ShiftMask,                XK_Page_Up,                  tagmon,            {.i = +1 } },
	{ MODKEY,                          XK_0,                        view,              {.ui = ~0 } },
	{ MODKEY|ShiftMask,                XK_0,                        tag,               {.ui = ~0 } },
	{ MODKEY,                          XK_Tab,                      view,              {0} },
	{ MODKEY|ShiftMask,                XK_Tab,                      setlayout,         {0} },
	{ MODKEY,                          XK_bracketleft,              setgap,            {.i = -2 } },
	{ MODKEY,                          XK_bracketright,             setgap,            {.i = +2 } },
	{ MODKEY|ShiftMask,                XK_bracketleft,              setmargin,         {.i = -2 } },
	{ MODKEY|ShiftMask,                XK_bracketright,             setmargin,         {.i = +2 } },
	{ MODKEY,                          XK_Escape,                   killclient,        {0} },
	{ MODKEY|ShiftMask,                XK_Escape,                   quit,              {0} },
	TAGKEYS(                           XK_1,                                           0)
	TAGKEYS(                           XK_2,                                           1)
	TAGKEYS(                           XK_3,                                           2)
	TAGKEYS(                           XK_4,                                           3)
	TAGKEYS(                           XK_5,                                           4)
	TAGKEYS(                           XK_6,                                           5)
	TAGKEYS(                           XK_7,                                           6)
	TAGKEYS(                           XK_8,                                           7)
	TAGKEYS(                           XK_9,                                           8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkStatusText,        0,              Button1,        spawn,          {.v = simplestatuscmd } },
	{ ClkStatusText,        0,              Button3,        spawn,          {.v = advancedstatuscmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
};

